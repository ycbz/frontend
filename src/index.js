import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router-dom';
import { Routes } from 'react-router-dom';
import './assets/scss/style.scss';
import Button from './components/button/Button';
import Footer from './components/footer/Footer';
import Header from './components/header/Header';
import Home from './pages/home/Home';
import Login from './pages/login/Login';
import Map from './pages/map/Map';
import Nav from './components/nav/Nav';
import React from 'react';
import ReactDOM from 'react-dom/client';
import Register from './pages/register/Register';
import reportWebVitals from './reportWebVitals';
import Title from './components/title/Title';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route>
          <Route path='/' element={<Login />} />
          <Route path='/accueil' element={<Home />} />
          <Route path='/button' element={<Button />} />
          <Route path='/carte' element={<Map />} />
          <Route path='/connexion' element={<Login />} />
          <Route path='/footer' element={<Footer />} />
          <Route path='/header' element={<Header />} />
          <Route path='/inscription' element={<Register />} />
          <Route path='/nav' element={<Nav />} />
          <Route path='/title' element={<Title />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your Home, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
