import { Link } from "react-router-dom";
import "../../assets/scss/style.scss";
import "./Header.module.scss";
import Button from "../button/Button";
import Nav from "../nav/Nav";
import React from "react";

function Header(props) {
    return (
        <header>
            <nav>
                <ul>
                    <li>
                        <Link to="/accueil" style={{ textDecoration: 'none' }}>
                            <img src="Logo.png" alt="Logo" />
                        </Link>
                    </li>
                    <li>
                        <Link to={props.to}>
                            <Nav text={props.text}></Nav>
                        </Link>
                    </li>
                    <li>
                        <Link to="/connexion">
                            <Button text="Se connecter"></Button>
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Header;
