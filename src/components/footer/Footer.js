import { Link } from "react-router-dom";
import "../../assets/scss/style.scss";
import React from "react";
import style from "./Footer.module.scss";

function Footer() {
  return (
    <footer>
      <div className={style.row1}>
        <div className={style.column1}>
          <Link to="/">
            <img src="Logo.png" alt="Logo" />
          </Link>
        </div>
        <div className={style.column2}>
          <h3>Contact</h3>
          <p>8, rue Jean Baptiste Fabre</p>
          <p>43000 Le Puy-en-Velay</p>
          <a href="mailto:yael.cubizolles@etu.uca.fr">
            <p>yael.cubizolles@etu.uca.fr</p>
          </a>
          <a href="tel:+33.6.02.67.54.67">
            <p>+33 6 02 67 54 67</p>
          </a>
        </div>
        <div className={style.column3}>
          <h3>Liens</h3>
          <p>
            <Link to="/accueil">Accueil</Link>
          </p>
          <p>
            <Link to="/carte">Carte</Link>
          </p>
          <p>
            <Link to="/inscription">S'inscrire</Link>
          </p>
          <p>
            <Link to="/connexion">Se connecter</Link>
          </p>
        </div>
      </div>
      <div className={style.row2}>
        <p className="text-center">
          &copy; 2023 <Link to="/accueil">francemusees.fr</Link> - Tous droits
          réservés
        </p>
      </div>
    </footer>
  );
}

export default Footer;
