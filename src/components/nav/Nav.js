import React from "react";
import style from "./Nav.module.scss";

function Nav(props) {
    return (
        <button className={style.nav}>{props.text}</button>
    )
}

export default Nav;
