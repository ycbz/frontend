import "../../assets/scss/style.scss";
import React from "react";
import style from "./Title.module.scss";

function Title(props) {
    return (
        <div className={style.title}>
            <h1>{props.title}</h1>
        </div>
    );
}

export default Title;
