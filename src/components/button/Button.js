import "../../assets/scss/style.scss";
import React from "react";
import style from "./Button.module.scss";

function Button(props) {
    return (
        <button className={style.button}>{props.text}</button>
    )
}

export default Button;
