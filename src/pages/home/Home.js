import { Link } from 'react-router-dom';
import "../../assets/scss/style.scss";
import "./Home.scss";
import Footer from "../../components/footer/Footer";
import Header from "../../components/header/Header";
import React from "react";
import Title from "../../components/title/Title";

function Home() {
  return (
    <>
      <Header text="Carte" to="/carte" />
      <Title title="Accueil" />
      <div className="text-accueil">
        <p>Voici mon projet d'application web sur les musées de France, qui inclut deux jeux de données : la liste des musées de France et leur localisation, ainsi que leur taux de fréquentation en 2018. J'ai choisi ces données car je pense qu'elles sont importantes pour les utilisateurs qui souhaitent découvrir les musées de leur région ou département.</p>
        <p>Mon application permet aux utilisateurs de se connecter et d'accéder à une carte interactive qui affiche les musées selon la région ou le département sélectionné ; cela leur permet de visualiser rapidement et facilement les musées de leur choix.</p>
        <p>Les musées sont des institutions culturelles importantes qui jouent un rôle clé dans la conservation et la promotion du patrimoine culturel de la France. En incluant le taux de fréquentation, les utilisateurs peuvent mieux comprendre quels musées sont les plus populaires et planifier leur visite en conséquence.</p>
        <p>Enfin, je pense qu'il serait intéressant d'intégrer d'autres fonctionnalités à l'application pour améliorer l'expérience utilisateur : comme permettre aux utilisateurs de sauvegarder leurs préférences et de suivre les musées qu'ils souhaitent visiter, une section "Événements" qui répertorie les expositions temporaires, les conférences et autres événements organisés par les musées ou encore une fonction de recherche pour permettre aux utilisateurs de voir si une ville possède un musée à visiter.</p>
        <p><Link to="/accueil">francemusees.fr</Link> est un excellent moyen de promouvoir la richesse culturelle de la France et de faciliter l'accès à ses musées. Ma carte interactive et mon système de connexion sont des atouts qui rendent l'application utile et conviviale pour les utilisateurs.</p>
      </div>
      <Footer />
    </>
  );
}

export default Home;
