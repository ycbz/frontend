import { Link } from 'react-router-dom';
import "../../assets/scss/style.scss";
import "./Login.scss";
import Button from "../../components/button/Button";
import React from "react";
import Title from "../../components/title/Title";

function Login() {
    return (
        <div>
            <Title title="Connexion" />
            <form>
                <label for="username">Nom d'utilisateur :</label>
                <input type="text" id="username" name="username" required></input>

                <label for="password">Mot de passe :</label>
                <input type="password" id="password" name="password" required></input>

                <Button text="Se connecter"></Button>

                <div className="text">
                    <p>Pas encore de compte ?</p>
                    <p><Link to="/inscription">Inscrivez-vous.</Link></p>
                </div>

            </form>
        </div>
    );
}

export default Login;