import "../../assets/scss/style.scss";
import "leaflet/dist/leaflet.css";
import "./Map.scss";
import Footer from "../../components/footer/Footer";
import Header from "../../components/header/Header";
import L from "leaflet";
import React, { useEffect, useState } from "react";
import Title from "../../components/title/Title";

function Map() {
  const [hoverData, setHoverData] = useState(null);

  useEffect(() => {
    const map = L.map("map").setView([46.5, 2.6], 6);

    fetch("https://france-geojson.gregoiredavid.fr/repo/regions.geojson")
      .then((response) => response.json())
      .then((data) => {
        L.geoJSON(data, {
          style: { color: "#222", weight: 1 },
          onEachFeature: function (feature, layer) {
            layer.on({
              mouseover: function (e) {
                layer.setStyle({ color: "#27dcc4" });
                fetch(
                  "http://127.0.0.1:8000/liste-region/" + feature.properties.nom
                )
                  .then((response) => response.json())
                  .then((data) => {
                    console.log(data);
                  });
              },

              mouseout: function (e) {
                setHoverData(null);
                layer.setStyle({ color: "#222" });
              },
            });
          },
        }).addTo(map);
      });

    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(
      map
    );

    return () => {
      map.remove();
    };
  }, []);

  return (
    <>
      <Header text="Accueil" to="/accueil" />
      <Title title="Carte" />
      <div id="map"></div>
      <Footer />
    </>
  );
}

export default Map;
