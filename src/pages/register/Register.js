import { Link } from "react-router-dom";
import "../../assets/scss/style.scss";
import "./Register.scss";
import Button from "../../components/button/Button";
import React from "react";
import Title from "../../components/title/Title";

function Register() {
    return (
        <div>
            <Title title="Inscription" />
            <form className="form-register">
                <div className="container">
                    <label className="label-register1" for="name">Nom d'utilisateur :</label>
                    <input className="input-register" type="text" id="name" name="name" required></input>

                    <label className="label-register2" for="email">Courriel :</label>
                    <input className="input-register" type="email" id="email" name="email" required></input>
                </div>

                <label for="password">Mot de passe :</label>
                <input type="password" id="password" name="password" required></input>

                <Button text="S'inscrire"></Button>

                <div className="text">
                    <p>Déjà inscrit ?</p>
                    <p><Link to="/connexion">Se connecter.</Link></p>
                </div>
            </form>
        </div>
    );
}

export default Register;
